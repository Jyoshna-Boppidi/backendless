//
//  ViewController.swift
//  Northwest-KCProgrammingContestApp
//
//  Created by Boppidi,Jyoshna on 3/13/19.
//  Copyright © 2019 Boppidi,Jyoshna. All rights reserved.
//

import UIKit
class NewSchoolController: UIViewController {
    @IBOutlet weak var CoachNameTF: UITextField!
    @IBOutlet weak var SchoolNameTF: UITextField!
    var Nschool: School!
    override func viewDidLoad() {
        super.viewDidLoad()
}
    @IBAction func done(_ sender: Any) {
let schoolName = SchoolNameTF.text!
let coachName = CoachNameTF.text!
        Schools.shared.saveSchool(name: SchoolNameTF.text!, coach: CoachNameTF.text!)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

