//
//  TeamsTableViewController.swift
//  Northwest-KCProgrammingContestApp
//
//  Created by Boppidi,Jyoshna on 3/13/19.
//  Copyright © 2019 Boppidi,Jyoshna. All rights reserved.
//

import UIKit
class TeamsTableViewController: UITableViewController {
var schl: School!
override func viewDidLoad() {
        super.viewDidLoad()
    }
override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
}
override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schl.teams.count
}
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamview", for: indexPath)
        cell.textLabel?.text = schl.teams[indexPath.row].name
        return cell
}
override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = schl.name
        tableView.reloadData()
}
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
if segue.identifier == "team"{
let studentsVC = segue.destination as! StudentsViewController
            studentsVC.team = schl.teams[tableView.indexPathForSelectedRow!.row]
}
        else if segue.identifier == "Nteam"{
            let newTeamsVC = segue.destination as! NewTeamViewController
            newTeamsVC.school = schl
}
}
}
