//
//  NewTeamViewController.swift
//  Northwest-KCProgrammingContestApp
//
//  Created by Boppidi,Jyoshna on 3/13/19.
//  Copyright © 2019 Boppidi,Jyoshna. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var STU0TF: UITextField!
    @IBOutlet weak var STU1TF: UITextField!
    @IBOutlet weak var STU2TF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
}
    var school: School!
    @IBAction func done(_ sender: Any) {
        let team = Team(name: nameTF.text!, students:[STU0TF.text!,STU1TF.text!,STU2TF.text!])
        if nameTF.text != "" && (STU0TF.text != "" || STU1TF.text != "" || STU2TF.text != "")  {
    Schools.shared.saveTeamForSelectedSchool(school: school, team: team)
    self.dismiss(animated: true, completion: nil)
        }else{
            displayMessage()
        }
    }
func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
}
}
